PROJECT="omnisend-backbone-lab"
DATAFLOW_PROJECT="$PROJECT"
BQ_PROJECT="$PROJECT"
BQ_DATASET="$PROJECT"
PUBSUB_PROJECT="$PROJECT"
PUBSUB_SUBSCRIBTION="paulius-test-sub"
REGION="us-central1"
BUCKET="paulius-test-bucker"
SA_FILE="$PWD/sa_key.json"
SA_ACCOUNT="data-flow-pipeline-test@$PROJECT.iam.gserviceaccount.com"
JOB_NAME="dataflow-libraryapp-job"
MAX_WORKERS=1
WORKER_MACHINE_TYPE="n1-standard-2"
RUNNER="DirectRunner"
#RUNNER="DataflowRunner"

TEMP_LOCATION="gs://$BUCKET/tmp/"
STAGING_LOCATION="gs://$BUCKET/staging/"

ARGS="--runner=$RUNNER
--tempLocation=$TEMP_LOCATION 
--stagingLocation=$STAGING_LOCATION 
--region=$REGION 
--enableStreamingEngine 
--numWorkers=1
--jobName=$JOB_NAME
--usePublicIps=false
--maxNumWorkers=$MAX_WORKERS
--autoscalingAlgorithm=THROUGHPUT_BASED
--project=$DATAFLOW_PROJECT 
--BQProject=$BQ_PROJECT 
--BQDataset=$BQ_DATASET 
--pubSubProject=$PUBSUB_PROJECT 
--subscription=$PUBSUB_SUBSCRIBTION
--bucket=$BUCKET
--serviceAccount=$SA_ACCOUNT
--streaming=true
--workerMachineType=$WORKER_MACHINE_TYPE"

echo $ARGS

if [ ! -f "$SA_FILE" ]; then
    echo "$SA_FILE does not exist."
    exit
fi

export GOOGLE_APPLICATION_CREDENTIALS=$SA_FILE
echo $GOOGLE_APPLICATION_CREDENTIALS

echo ./gradlew clean run -Pargs="$ARGS"

./gradlew clean run -Pargs="$ARGS"

unset GOOGLE_APPLICATION_CREDENTIALS